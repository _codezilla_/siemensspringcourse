#pragma once
#include <spring\Framework\IScene.h>

namespace Spring
{
	class BaseScene :public IScene
	{
	public:
		QWidget *centralWidget;
		explicit BaseScene(const std::string& ac_szSceneName);
		virtual void createScene() override;
		virtual void release() override;
		void createGUI();
	};
}
