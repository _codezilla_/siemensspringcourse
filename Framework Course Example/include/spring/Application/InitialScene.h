#pragma once
#include <spring\Framework\IScene.h>
#include <string>
#include <qwidget.h>
#include <qobject.h>
#include <qgridlayout.h>
#include <qpushbutton>
#include <qlineedit.h>
#include <qlabel.h>
#include <qspinbox.h>
#include <qapplication.h>
namespace Spring
{

	class InitialScene : public IScene
	{
		Q_OBJECT
	public:
		QWidget *centralWidget;
		explicit InitialScene(const std::string& ac_szSceneName);
		virtual void createScene() override;
		virtual void release() override;
		void createGUI();

		QGridLayout *gridLayout_2;
		QGridLayout *gridLayout;
		QLabel *label_3;
		QLabel *label;
		QLabel *label_2;
		QLabel *label_4;
		QLineEdit *lineEdit;
		QSpacerItem *horizontalSpacer;
		QSpacerItem *verticalSpacer_2;
		QSpacerItem *horizontalSpacer_2;
		QSpacerItem *verticalSpacer;
		QSpinBox *spinBox;
		QDoubleSpinBox *doubleSpinBox;
		QSpinBox *spinBox_2;
		QPushButton *pushButton;



		private slots :
		void mf_OkButton();


	};
}
