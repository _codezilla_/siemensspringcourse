#pragma once
#include <spring\Framework\IApplicationModel.h>
#include "global.h"
#include "InitialScene.h"

namespace Spring
{
	class Application_EXPORT_IMPORT_API CApplicationModel : public IApplicationModel
	{
	public:
		explicit CApplicationModel();

		virtual void defineScene();

		virtual void defineInitialScene();

		virtual void defineTransientData();


	};
}
