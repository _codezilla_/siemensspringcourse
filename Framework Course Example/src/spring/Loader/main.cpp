#include <spring\Application\ApplicationModel.h>
#include <spring\Framework\Application.h>
#include <qapplication.h>

int main(int argc, char **argv)
{
	QApplication app(argc, argv);
	Spring::Application& application = Spring::Application::getInstance();
	Spring::CApplicationModel model;
	//Spring::IApplicationModel *imodel = new Spring::CApplicationModel();
	application.setApplicationModel(&model);
	application.start("Siemens 2k17", 420, 420);


	app.exec();


	return 0;
}