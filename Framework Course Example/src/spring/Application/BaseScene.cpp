#include "..\..\..\include\spring\Application\BaseScene.h"

namespace Spring
{
	BaseScene::BaseScene(const std::string & ac_szSceneName):IScene(ac_szSceneName)
	{

	}
	void BaseScene::createScene()
	{
		m_uMainWindow->setWindowTitle(boost::any_cast<QString>(m_TransientDataCollection["ApplicationName"]));
		createGUI();
		
	}
	void BaseScene::release()
	{
	}
	void BaseScene::createGUI()
	{

	}
}
