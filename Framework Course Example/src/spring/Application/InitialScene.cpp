#include "..\..\..\include\spring\Application\InitialScene.h"

#include <iostream>
namespace Spring
{
	InitialScene::InitialScene(const std::string & ac_szSceneName) :IScene(ac_szSceneName)
	{
	}
	void InitialScene::createScene()
	{
		createGUI();
	}
	void InitialScene::mf_OkButton()
	{
		m_TransientDataCollection.erase("ApplicationName");
		m_TransientDataCollection.emplace("ApplicationName",lineEdit->text());
		m_TransientDataCollection.erase("SampleRate");
		m_TransientDataCollection.emplace("SampleRate", spinBox->value());
		m_TransientDataCollection.erase("DisplayTime");
		m_TransientDataCollection.emplace("DisplayTime", doubleSpinBox->value());
		m_TransientDataCollection.erase("RefreshRate");
		m_TransientDataCollection.emplace("RefreshRate", spinBox_2->value());

		//std::cout << m_TransientDataCollection["DisplayTime"];

		emit SceneChange("BaseScene");

	}

	void InitialScene::release()
	{
		delete centralWidget;
	}
	void InitialScene::createGUI()
	{
		m_uMainWindow->resize(1113, 726);

		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		gridLayout_2 = new QGridLayout(centralWidget);
		gridLayout_2->setSpacing(6);
		gridLayout_2->setContentsMargins(11, 11, 11, 11);
		gridLayout_2->setObjectName(QStringLiteral("gridLayout_2"));
		gridLayout = new QGridLayout();
		gridLayout->setSpacing(6);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));
		gridLayout->setSizeConstraint(QLayout::SetDefaultConstraint);
		gridLayout->setContentsMargins(0, -1, -1, 0);
		label_3 = new QLabel(centralWidget);
		label_3->setObjectName(QStringLiteral("label_3"));

		gridLayout->addWidget(label_3, 2, 1, 1, 1);

		label = new QLabel(centralWidget);
		label->setObjectName(QStringLiteral("label"));

		gridLayout->addWidget(label, 4, 1, 1, 1);

		label_2 = new QLabel(centralWidget);
		label_2->setObjectName(QStringLiteral("label_2"));

		gridLayout->addWidget(label_2, 1, 1, 1, 1);

		label_4 = new QLabel(centralWidget);
		label_4->setObjectName(QStringLiteral("label_4"));

		gridLayout->addWidget(label_4, 3, 1, 1, 1);

		lineEdit = new QLineEdit(centralWidget);
		lineEdit->setObjectName(QStringLiteral("lineEdit"));

		lineEdit->setText(boost::any_cast<QString>(m_TransientDataCollection["ApplicationName"]));


		gridLayout->addWidget(lineEdit, 1, 2, 1, 1);

		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer, 2, 0, 1, 1);

		verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout->addItem(verticalSpacer_2, 5, 1, 1, 1);

		horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer_2, 2, 3, 1, 1);

		verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		gridLayout->addItem(verticalSpacer, 0, 1, 1, 1);

		spinBox = new QSpinBox(centralWidget);
		spinBox->setObjectName(QStringLiteral("spinBox"));
		spinBox->setMaximum(900000);
		spinBox->setValue(boost::any_cast<int>(m_TransientDataCollection["SampleRate"]));

		gridLayout->addWidget(spinBox, 2, 2, 1, 1);

		doubleSpinBox = new QDoubleSpinBox(centralWidget);
		doubleSpinBox->setObjectName(QStringLiteral("doubleSpinBox"));
		doubleSpinBox->setValue(boost::any_cast<double>(m_TransientDataCollection["DisplayTime"]));

		gridLayout->addWidget(doubleSpinBox, 3, 2, 1, 1);

		spinBox_2 = new QSpinBox(centralWidget);
		spinBox_2->setObjectName(QStringLiteral("spinBox_2"));
		spinBox_2->setValue(boost::any_cast<int>(m_TransientDataCollection["RefreshRate"]));
		gridLayout->addWidget(spinBox_2, 4, 2, 1, 1);

		pushButton = new QPushButton(centralWidget);
		pushButton->setObjectName(QStringLiteral("pushButton"));
		connect(pushButton, SIGNAL(clicked()), this, SLOT(mf_OkButton()));

		gridLayout->addWidget(pushButton, 5, 3, 1, 1);


		gridLayout_2->addLayout(gridLayout, 0, 0, 1, 1);

		m_uMainWindow->setCentralWidget(centralWidget);

		m_uMainWindow->setWindowTitle(QApplication::translate("MainWindow", "MainWindow", Q_NULLPTR));
		label_3->setText(QApplication::translate("MainWindow", "Sample Rate", Q_NULLPTR));
		label->setText(QApplication::translate("MainWindow", "Refresh Rate", Q_NULLPTR));
		label_2->setText(QApplication::translate("MainWindow", "Application Name", Q_NULLPTR));
		label_4->setText(QApplication::translate("MainWindow", "Display Time", Q_NULLPTR));
		pushButton->setText(QApplication::translate("MainWindow", "OK", Q_NULLPTR));


	}
}
