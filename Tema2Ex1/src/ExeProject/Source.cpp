#include <iostream>
#include <windows.h>
#include "FirstStaticLib.h"
typedef void(__cdecl *OnStartProcedure)();
typedef void(__cdecl *OnHelloProcedure)();

int main()
{
	HINSTANCE hModule = LoadLibrary(TEXT("MyFirstDLL.dll"));
	HINSTANCE hModule2 = LoadLibrary(TEXT("MySecondDLL.dll"));
	OnStartProcedure pOnStart = (OnStartProcedure)GetProcAddress(hModule, "OnStart");
	OnHelloProcedure pOnHello = (OnHelloProcedure)GetProcAddress(hModule2, "OnHello");

	if (pOnStart)
		pOnStart();
	if (pOnHello)
		pOnHello();
	FirstStaticLb::SayHello();

	return 0;
}