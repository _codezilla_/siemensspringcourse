#pragma once

#include <qvector.h>
#include <qserialport.h>
#include <qiodevice.h>
#include <qserialportinfo.h>

class SerialInput : public QSerialPort {
public:
	SerialInput(QSerialPortInfo info);
	qint64 writeData(const char *data, qint64 len) override;
	qint64 readData(char *data, qint64 len) override;
	~SerialInput();
private:
};

