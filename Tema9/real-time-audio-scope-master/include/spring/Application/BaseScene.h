#pragma once

#include "spring/Framework/IScene.h"
#include <iostream>
#include <QtWidgets/QPushButton>
#include <spring\Application\SerialInput.h>
#include "../third_party/QCustomPlot/qcustomplot.h"
#include "spring/Application/MonoInput.h"
#include <QAudioInput>

namespace Spring
{
	class CBaseScene : public IScene
	{
		Q_OBJECT

	public:
		CBaseScene();
		~CBaseScene();
		void createScene() override;
		void release() override;

	private:
		QWidget *centralWidget;
		QPushButton *pushButtonStart;
		QPushButton *pushButtonStop;
		QPushButton *pushButtonBack;
		QCustomPlot *plot;
		QTimer		*timer;
		bool		isPlotStarted;

		QVector<double> xAxis;
		QVector<double> yAxis;
		bool pastFirstEntry = false;
		QSerialPort* serialPort;

		private slots:
		void mf_ReturnToInitialScene();
		void mf_PlotInput();
		void mf_CleanPlot();
		void mf_StartTimer();
		void mf_StopTimer();
	};
}
