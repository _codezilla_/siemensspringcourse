#include "spring/Application/BaseScene.h"
#include <QtWidgets/QGridLayout>
#include <qapplication.h>

namespace Spring
{
	CBaseScene::CBaseScene()
		: IScene(std::string("BaseScene")),
		isPlotStarted(false)
	{
		timer = new QTimer(this);
		QObject::connect(timer, SIGNAL(timeout()), this, SLOT(mf_PlotInput()));
	}

	void Spring::CBaseScene::createScene()
	{
		QMainWindow* pMainWindow = uGetWindow().get();

		centralWidget = new QWidget(pMainWindow);
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		QGridLayout *gridLayout = new QGridLayout(centralWidget);
		gridLayout->setSpacing(6);
		gridLayout->setContentsMargins(11, 11, 11, 11);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));

		QSpacerItem *horizontalSpacerButtons = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacerButtons, 3, 0, 1, 1);

		pushButtonStart = new QPushButton(centralWidget);
		pushButtonStart->setObjectName(QStringLiteral("pushButtonStart"));

		gridLayout->addWidget(pushButtonStart, 3, 1, 1, 1);

		plot = new QCustomPlot(centralWidget);
		plot->setObjectName(QStringLiteral("plot"));
		gridLayout->addWidget(plot, 2, 0, 1, 4);

		pushButtonStop = new QPushButton(centralWidget);
		pushButtonStop->setObjectName(QStringLiteral("pushButtonStop"));

		gridLayout->addWidget(pushButtonStop, 3, 2, 1, 1);

		pushButtonBack = new QPushButton(centralWidget);
		pushButtonBack->setObjectName(QStringLiteral("pushButtonBack"));

		gridLayout->addWidget(pushButtonBack, 3, 3, 1, 1);

		pMainWindow->setCentralWidget(centralWidget);

		pMainWindow->setWindowTitle(QApplication::translate("DisplayWindow", "DisplayWindow", Q_NULLPTR));
		pushButtonStart->setText(QApplication::translate("DisplayWindow", "&Start", Q_NULLPTR));
		pushButtonStop->setText(QApplication::translate("DisplayWindow", "Sto&p", Q_NULLPTR));
		pushButtonBack->setText(QApplication::translate("DisplayWindow", "&Back", Q_NULLPTR));

		QObject::connect(pushButtonBack, SIGNAL(released()), this, SLOT(mf_ReturnToInitialScene()));

		QObject::connect(pushButtonStart, SIGNAL(released()), this, SLOT(mf_StartTimer()));

		QObject::connect(pushButtonStop, SIGNAL(released()), this, SLOT(mf_StopTimer()));

		plot->addGraph();

		unsigned sampleRate = boost::any_cast<unsigned>(m_TransientDataCollection.find("SampleRate")->second);
		double displayTime = boost::any_cast<double>(m_TransientDataCollection.find("DisplayTime")->second);


		foreach(const QSerialPortInfo &info, QSerialPortInfo::availablePorts())
		{
			std::cout << info.portName().toStdString() << std::endl;
			std::cout << info.description().toStdString() << std::endl;
			std::cout << info.manufacturer().toStdString() << std::endl;

			serialPort = new QSerialPort(info);
			serialPort->setBaudRate(9600);
			serialPort->open(QIODevice::ReadOnly);
		}

		for (int i = 0; i < 1100; i++)
		{
			xAxis.push_back(i);
		}
	}

	void CBaseScene::release()
	{
		delete centralWidget;
		centralWidget = nullptr;
		// the rest of the member qt controls are now dangling pointers; let's clear them out
		pushButtonStart = nullptr;
		pushButtonStop = nullptr;
		pushButtonBack = nullptr;
		plot = nullptr;

		delete serialPort;
		serialPort = nullptr;
	}

	CBaseScene::~CBaseScene()
	{
		delete timer;
		timer = nullptr;
	}
	void CBaseScene::mf_ReturnToInitialScene()
	{
		emit SceneChange("InitialScene");
	}

	void CBaseScene::mf_PlotInput()
	{
		yAxis.clear();
		QByteArray data = serialPort->readAll();
		std::string datastring = data.toStdString();
		std::string aux = datastring;
		char separatori[] = "\r\n";
		char *token;
		token = strtok(&aux[0], separatori);

		while (token != NULL)
		{
			std::string toPush = token;
			if (toPush != "")
			{
				yAxis.push_back(std::stod(toPush));
			}
			token = strtok(NULL, separatori);
		}
		if (pastFirstEntry == false)
		{
			plot->xAxis->setRange(0, 98);
			plot->yAxis->setRange(0, 1100);
			pastFirstEntry = true;
		}
		std::cout << yAxis.size() << std::endl;
		plot->graph(0)->data()->clear();

		QVector<double> xAxisClone(xAxis);
		xAxisClone.resize(yAxis.size());
		plot->graph(0)->setData(xAxisClone, yAxis);

		plot->replot();
	}

	void CBaseScene::mf_CleanPlot()
	{
		plot->graph(0)->data()->clear();
		plot->replot();
	}

	void CBaseScene::mf_StartTimer()
	{
		if (!isPlotStarted)
		{
			unsigned refreshRate = boost::any_cast<unsigned>(m_TransientDataCollection.find("RefreshRate")->second);
			timer->start(1000 / refreshRate);
			isPlotStarted = true;
			pushButtonBack->setEnabled(false);
		}
	}

	void CBaseScene::mf_StopTimer()
	{
		if (isPlotStarted)
		{
			timer->stop();
			isPlotStarted = false;
			mf_CleanPlot();
			pushButtonBack->setEnabled(true);
		}
	}
}
