#include <spring\Application\SerialInput.h>
#include <iostream>
#include <qendian.h>


SerialInput::SerialInput(QSerialPortInfo info)
{
	setPort(info);
	open(QIODevice::ReadOnly);
	setBaudRate(9600);
	setDataBits(QSerialPort::Data8);
}

qint64 SerialInput::writeData(const char * data, qint64 len)
{
	return len;
}


qint64 SerialInput::readData(char * data, qint64 len)
{
	const auto *ptr = data;

	for (auto i = 0; i < len; ++i)
	{
		qint32 value = 0;
		value = qFromLittleEndian<qint16>(ptr);
		ptr++;
	}
	return len;
}


SerialInput::~SerialInput()
{
}
