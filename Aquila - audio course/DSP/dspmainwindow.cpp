#include "dspmainwindow.h"
#include "ui_dspmainwindow.h"
#include <math.h>
#include <aquila/global.h>
#include <aquila/transform/FftFactory.h>

DSPMainWindow::DSPMainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::DSPMainWindow)
{
	ui->setupUi(this);

	QVector<double> xAxis(44100);
	//for (int i = 0; i < 44100; i++)
	//{
	//	xAxis[i] = i / 44100.0;
	//}

	QVector<double> yAxis(44100);
	//for (int i = 0; i < 44100; i++)
	//{
	//	yAxis[i] = 1 * sin(2 * M_PI * 5000 * xAxis[i]);
	//}

	//ui->plot->addGraph();
	//ui->plot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
	//ui->plot->graph(0)->setData(xAxis, yAxis);
	//ui->plot->xAxis->setRange(0, 0.1);
	//ui->plot->yAxis->setRange(-1, 1);
	//ui->plot->replot();

	//int FFTSize = 128;
	//double frequencyRes = 44100 / FFTSize;
	//auto fft = Aquila::FftFactory::getFft(FFTSize);
	//auto spectrum = fft->fft(yAxis.toStdVector().data());
	//QVector<double> amplitude, frequency;
	//int index = 0;
	//for each (auto item in spectrum)
	//{
	//	amplitude.push_back(pow(item.real(), 2) + pow(item.imag(), 2));
	//	frequency.push_back((index++)*frequencyRes);
	//}

	//ui->plot->addGraph();
	//ui->plot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsImpulse);
	//ui->plot->graph(0)->setData(frequency, amplitude);
	//ui->plot->xAxis->setRange(20, 20000);
	//ui->plot->yAxis->setRange(0, *std::max_element(amplitude.begin(), amplitude.end()));
	//ui->plot->replot();


	//squareWave(xAxis,yAxis);
	//frequencyDomain(xAxis, yAxis);
	//sawToothWave(xAxis, yAxis);
	//frequencyDomain(xAxis, yAxis);
	//twoSineWaves();
	//sweepWave(xAxis, yAxis);
	filters();
}

DSPMainWindow::~DSPMainWindow()
{
	delete ui;
}

void DSPMainWindow::sineWave(QVector<double>& xAxis, QVector<double>& yAxis, double A, double f)
{
	for (int i = 0; i < 44100; i++)
	{
		xAxis[i] = i / 44100.0;
	}

	for (int i = 0; i < 44100; i++)
	{
		yAxis[i] = A * sin(2 * M_PI * f* xAxis[i]);
	}
}

void DSPMainWindow::squareWave(QVector<double> &xAxis, QVector<double> &yAxis)
{
	for (int i = 0; i < 44100; i++)
	{
		xAxis[i] = i / 44100.0;
	}

	for (int i = 0; i < 44100; i++)
	{
		yAxis[i] = 0.5 * pow(-1, floor((2 * (xAxis[i] - 0)) / (1 / 200.0)));
	}
	//ui->plot->addGraph();
	//ui->plot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
	//ui->plot->graph(0)->setData(xAxis, yAxis);
	//ui->plot->xAxis->setRange(0, 0.1);
	//ui->plot->yAxis->setRange(-1, 1);
	//ui->plot->replot();


}

void DSPMainWindow::frequencyDomain(QVector<double> xAxis, QVector<double> yAxis)
{
	int FFTSize = 128;
	double frequencyRes = 44100 / FFTSize;
	auto fft = Aquila::FftFactory::getFft(FFTSize);
	auto spectrum = fft->fft(yAxis.toStdVector().data());
	QVector<double> amplitude, frequency;
	int index = 0;
	for each (auto item in spectrum)
	{
		amplitude.push_back(pow(item.real(), 2) + pow(item.imag(), 2));
		frequency.push_back((index++)*frequencyRes);
	}

	ui->plot->addGraph();
	ui->plot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsImpulse);
	ui->plot->graph(0)->setData(frequency, amplitude);
	ui->plot->xAxis->setRange(20, 20000);
	ui->plot->yAxis->setRange(0, *std::max_element(amplitude.begin(), amplitude.end()));
	ui->plot->replot();
}

void DSPMainWindow::sawToothWave(QVector<double>& xAxis, QVector<double>& yAxis)
{
	for (int i = 0; i < 44100; i++)
	{
		xAxis[i] = i / 44100.0;
	}

	for (int i = 0; i < 44100; i++)
	{
		yAxis[i] = 0.5 * frac(xAxis[i] / (1 / 200.0) + 0.7);
	}


}

void DSPMainWindow::plot(const QVector<double> xAxis, const QVector<double> yAxis)
{
	ui->plot->addGraph();
	ui->plot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
	ui->plot->graph(0)->setData(xAxis, yAxis);
	ui->plot->xAxis->setRange(0, 0.1);
	ui->plot->yAxis->setRange(-5, 5);
	ui->plot->replot();
}

void DSPMainWindow::twoSineWaves()
{
	QVector<double> xAxis1(44100);
	QVector<double> yAxis1(44100);
	QVector<double> xAxis2(44100);
	QVector<double> yAxis2(44100);

	sineWave(xAxis1, yAxis1, 1, 500);

	sineWave(xAxis2, yAxis2, 1, 300);

	QVector<double> xAxis3(44100);
	QVector<double> yAxis3(44100);

	for (int i = 0; i < 44100; i++)
	{
		xAxis3[i] = xAxis1[i];
		yAxis3[i] = yAxis1[i] + yAxis2[i];
	}

	//plot(xAxis3, yAxis3);
	frequencyDomain(xAxis3, yAxis3);
}

void DSPMainWindow::sweepWave(QVector<double>& xAxis, QVector<double>& yAxis)
{
	for (int i = 0; i < 44100; i++)
	{
		xAxis[i] = i / 44100.0;
	}
	double k = 10000;
	for (int i = 0; i < 44100; i++)
	{
		yAxis[i] = sin(6 + 2 * M_PI*(200 * xAxis[i] + (k / 2.0)*xAxis[i] * xAxis[i]));
	}
	//plot(xAxis, yAxis);
	frequencyDomain(xAxis, yAxis);
}



void DSPMainWindow::addSineWaves(QVector<double> xAxis1, QVector<double> yAxis1, QVector<double> xAxis2, QVector<double> yAxis2, QVector<double> &newXAxis, QVector<double> &newYAxis)
{

	for (int i = 0; i < 44100; i++)
	{
		newXAxis[i] = xAxis1[i];
		newYAxis[i] = yAxis1[i] + yAxis2[i];
	}

}

void DSPMainWindow::filters()
{
	QVector<double> xAxis1(44100);
	QVector<double> yAxis1(44100);
	QVector<double> xAxis2(44100);
	QVector<double> yAxis2(44100);
	QVector<double> xAxis3(44100);
	QVector<double> yAxis3(44100);
	QVector<double> xAxis4(44100);
	QVector<double> yAxis4(44100);
	QVector<double> xAxis5(44100);
	QVector<double> yAxis5(44100);
	QVector<double> xFinal(44100);
	QVector<double> yFinal(44100);

	sineWave(xAxis1, yAxis1, 1, 100);
	sineWave(xAxis2, yAxis2, 1, 440);
	sineWave(xAxis3, yAxis3, 1, 670);
	sineWave(xAxis4, yAxis4, 1, 800);
	sineWave(xAxis5, yAxis5, 1, 1200);

	addSineWaves(xAxis1, yAxis1, xAxis2, yAxis2, xFinal, yFinal);
	addSineWaves(xAxis3, yAxis3, xFinal, yFinal, xFinal, yFinal);
	addSineWaves(xAxis4, yAxis4, xFinal, yFinal, xFinal, yFinal);
	addSineWaves(xAxis5, yAxis5, xFinal, yFinal, xFinal, yFinal);

	//lowPass(500, yFinal);

	plot(xFinal, yFinal);

}

void DSPMainWindow::lowPass(double value, QVector<double> &axis)
{
	for (int i = 0; i < 44100; i++)
	{
		if (axis[i] < value)
			axis[i] = 0;
	}
}

void DSPMainWindow::highPass(double value, QVector<double> &axis)
{
	for (int i = 0; i < 44100; i++)
	{
		if (axis[i] > value)
			axis[i] = 0;
	}
}

void DSPMainWindow::bandPass(double min, double max, QVector<double> &axis)
{
	for (int i = 0; i < 44100; i++)
	{
		if (axis[i] > min && axis[i] < max)
			axis[i] = 0;
	}
}
