#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "qplaintextedit.h"
#include "qfiledialog.h"
#include <fstream>
MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);
	ui->tabWidget->addTab(new QPlainTextEdit, QString("New%0").arg(ui->tabWidget->count()));
	connect(ui->actionNew, SIGNAL(triggered()), this, SLOT(buttonClicked()));
	connect(ui->actionOpen, SIGNAL(triggered()), this, SLOT(open()));
	connect(ui->actionSave, SIGNAL(triggered()), this, SLOT(save()));
	connect(ui->actionSave_As, SIGNAL(triggered()), this, SLOT(saveAs()));


}

void MainWindow::buttonClicked()
{
	ui->tabWidget->addTab(new QPlainTextEdit, QString("New%0").arg(ui->tabWidget->count()));
}


std::string MainWindow::getTextCurrentSelectedWidgetText()
{
	auto *textBox = (QPlainTextEdit*)ui->tabWidget->currentWidget();
	auto text = textBox->toPlainText();
	return text.toStdString();
}


void MainWindow::open()
{
	auto fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
		"",
		tr("Text files (*.txt)"));
	std::string path = fileName.toStdString();


	std::ifstream in(path);


	auto i = path.length() - 1;
	for (; i > 0; i--)
	{
		if (path[i] == '/')
			break;
	}

	i++;
	std::string tabName;

	for (; i < path.length(); i++)
	{
		auto a = path[i];
		tabName += a;
	}


	ui->tabWidget->addTab(new QPlainTextEdit, QString(tabName.c_str()));
	ui->tabWidget->setCurrentIndex(ui->tabWidget->count() - 1);
	paths[ui->tabWidget->currentWidget()] = fileName;
	auto *textBox = (QPlainTextEdit*)ui->tabWidget->currentWidget();


	std::string text;
	std::string cuvant;
	while (std::getline(in, cuvant))
	{
		text += cuvant;
		text += '\n';
	}
	text[text.length() - 1] = '\0';

	textBox->setPlainText(QString(text.c_str()));

}

void MainWindow::saveAs()
{

	auto fileName = QFileDialog::getSaveFileName(this, tr("Save File"),
		"",
		tr("Text files (*.txt)"));
	std::string path = fileName.toStdString();
	std::ofstream out(path);

	auto i = path.length() - 1;
	for (; i > 0; i--)
	{
		if (path[i] == '/')
			break;
	}

	i++;
	std::string tabName;

	for (; i < path.length(); i++)
	{
		auto a = path[i];
		tabName += a;
	}


	paths[ui->tabWidget->currentWidget()] = fileName;
	auto tabIndex = ui->tabWidget->currentIndex();
	ui->tabWidget->setTabText(tabIndex,QString(tabName.c_str()));
	

	out << getTextCurrentSelectedWidgetText();

	out.close();
}

void MainWindow::save()
{
	auto toFind = paths.find(ui->tabWidget->currentWidget());

	if (toFind == paths.end())
	{
		saveAs();
	}
	else
	{
		std::ofstream out(paths[ui->tabWidget->currentWidget()].toStdString());
		out << getTextCurrentSelectedWidgetText();

	}
}


MainWindow::~MainWindow()
{
	delete ui;
}
