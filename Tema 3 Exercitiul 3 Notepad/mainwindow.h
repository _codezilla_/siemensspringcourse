#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <map>
#include <string>

namespace Ui {
	class MainWindow;
}

class MainWindow : public QMainWindow
{
	Q_OBJECT

public:
	explicit MainWindow(QWidget *parent = 0);
	~MainWindow();

	public slots:
	void buttonClicked();
	void open();
	void saveAs();
	void save();

private:
	Ui::MainWindow *ui;
	std::map<QWidget*, QString> paths;
	std::string getTextCurrentSelectedWidgetText();


};

#endif // MAINWINDOW_H
