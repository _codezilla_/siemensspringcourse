/********************************************************************************
** Form generated from reading UI file 'dspmainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DSPMAINWINDOW_H
#define UI_DSPMAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QGridLayout>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QSpacerItem>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_DSPMainWindow
{
public:
    QWidget *centralWidget;
    QGridLayout *gridLay;
    QCustomPlot *FrequencyDomainPlot;
    QHBoxLayout *horizontalLayout;
    QSpacerItem *horizontalSpacer_2;
    QVBoxLayout *verticalLayout;
    QDoubleSpinBox *SineWaveSpinBox1;
    QDoubleSpinBox *SineWaveSpinBox2;
    QDoubleSpinBox *SineWaveSpinBox3;
    QDoubleSpinBox *SineWaveSpinBox4;
    QDoubleSpinBox *SineWaveSpinBox5;
    QPushButton *pushButton;
    QSpacerItem *horizontalSpacer;
    QCustomPlot *NormalResultPlot;

    void setupUi(QMainWindow *DSPMainWindow)
    {
        if (DSPMainWindow->objectName().isEmpty())
            DSPMainWindow->setObjectName(QStringLiteral("DSPMainWindow"));
        DSPMainWindow->resize(690, 355);
        centralWidget = new QWidget(DSPMainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        gridLay = new QGridLayout(centralWidget);
        gridLay->setSpacing(5);
        gridLay->setContentsMargins(15, 15, 15, 15);
        gridLay->setObjectName(QStringLiteral("gridLay"));
        FrequencyDomainPlot = new QCustomPlot(centralWidget);
        FrequencyDomainPlot->setObjectName(QStringLiteral("FrequencyDomainPlot"));

        gridLay->addWidget(FrequencyDomainPlot, 2, 0, 1, 1);

        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(5);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer_2);

        verticalLayout = new QVBoxLayout();
        verticalLayout->setSpacing(5);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        SineWaveSpinBox1 = new QDoubleSpinBox(centralWidget);
        SineWaveSpinBox1->setObjectName(QStringLiteral("SineWaveSpinBox1"));
        SineWaveSpinBox1->setMaximum(110000);
        SineWaveSpinBox1->setValue(400);

        verticalLayout->addWidget(SineWaveSpinBox1);

        SineWaveSpinBox2 = new QDoubleSpinBox(centralWidget);
        SineWaveSpinBox2->setObjectName(QStringLiteral("SineWaveSpinBox2"));
        SineWaveSpinBox2->setMaximum(110000);
        SineWaveSpinBox2->setValue(700);

        verticalLayout->addWidget(SineWaveSpinBox2);

        SineWaveSpinBox3 = new QDoubleSpinBox(centralWidget);
        SineWaveSpinBox3->setObjectName(QStringLiteral("SineWaveSpinBox3"));
        SineWaveSpinBox3->setMaximum(110000);
        SineWaveSpinBox3->setValue(960);

        verticalLayout->addWidget(SineWaveSpinBox3);

        SineWaveSpinBox4 = new QDoubleSpinBox(centralWidget);
        SineWaveSpinBox4->setObjectName(QStringLiteral("SineWaveSpinBox4"));
        SineWaveSpinBox4->setMaximum(110000);
        SineWaveSpinBox4->setValue(1000);

        verticalLayout->addWidget(SineWaveSpinBox4);

        SineWaveSpinBox5 = new QDoubleSpinBox(centralWidget);
        SineWaveSpinBox5->setObjectName(QStringLiteral("SineWaveSpinBox5"));
        SineWaveSpinBox5->setMaximum(110000);
        SineWaveSpinBox5->setValue(1100);

        verticalLayout->addWidget(SineWaveSpinBox5);

        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        verticalLayout->addWidget(pushButton);


        horizontalLayout->addLayout(verticalLayout);

        horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

        horizontalLayout->addItem(horizontalSpacer);


        gridLay->addLayout(horizontalLayout, 0, 0, 1, 1);

        NormalResultPlot = new QCustomPlot(centralWidget);
        NormalResultPlot->setObjectName(QStringLiteral("NormalResultPlot"));

        gridLay->addWidget(NormalResultPlot, 1, 0, 1, 1);

        DSPMainWindow->setCentralWidget(centralWidget);

        retranslateUi(DSPMainWindow);

        QMetaObject::connectSlotsByName(DSPMainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *DSPMainWindow)
    {
        DSPMainWindow->setWindowTitle(QApplication::translate("DSPMainWindow", "Main Window", Q_NULLPTR));
        pushButton->setText(QApplication::translate("DSPMainWindow", "Generate ", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class DSPMainWindow: public Ui_DSPMainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DSPMAINWINDOW_H
