#include "dspmainwindow.h"
#include "ui_dspmainwindow.h"
#include <math.h>
#include <aquila/global.h>
#include <aquila/transform/FftFactory.h>
#include <qmessagebox.h>

DSPMainWindow::DSPMainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::DSPMainWindow)
{
	ui->setupUi(this);

	connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(generateButtonMethod()));

}


void DSPMainWindow::generateButtonMethod()
{


	double frequency1 = ui->SineWaveSpinBox1->text().toDouble();
	double frequency2 = ui->SineWaveSpinBox2->text().toDouble();
	double frequency3 = ui->SineWaveSpinBox3->text().toDouble();
	double frequency4 = ui->SineWaveSpinBox4->text().toDouble();
	double frequency5 = ui->SineWaveSpinBox5->text().toDouble();

	QVector<double> xAxis1(44100);
	QVector<double> yAxis1(44100);

	QVector<double> xAxis2(44100);
	QVector<double> yAxis2(44100);

	QVector<double> xAxis3(44100);
	QVector<double> yAxis3(44100);

	QVector<double> xAxis4(44100);
	QVector<double> yAxis4(44100);

	QVector<double> xAxis5(44100);
	QVector<double> yAxis5(44100);

	QVector<double> xFinal(44100);
	QVector<double> yFinal(44100);

	sineWave(xAxis1, yAxis1, 1, frequency1);
	sineWave(xAxis2, yAxis2, 1, frequency2);
	sineWave(xAxis3, yAxis3, 1, frequency3);
	sineWave(xAxis4, yAxis4, 1, frequency4);
	sineWave(xAxis5, yAxis5, 1, frequency5);

	addSineWaves(xAxis1, yAxis1, xAxis2, yAxis2, xFinal, yFinal);
	addSineWaves(xAxis3, yAxis3, xFinal, yFinal, xFinal, yFinal);
	addSineWaves(xAxis4, yAxis4, xFinal, yFinal, xFinal, yFinal);
	addSineWaves(xAxis5, yAxis5, xFinal, yFinal, xFinal, yFinal);

	plot(xFinal, yFinal);
	frequencyDomain(xFinal, yFinal);
	
}


DSPMainWindow::~DSPMainWindow()
{
	delete ui;
}

void DSPMainWindow::sineWave(QVector<double>& xAxis, QVector<double>& yAxis, double A, double f)
{
	for (int i = 0; i < 44100; i++)
	{
		xAxis[i] = i / 44100.0;
	}

	for (int i = 0; i < 44100; i++)
	{
		yAxis[i] = A * sin(2 * M_PI * f* xAxis[i]);
	}
}


void DSPMainWindow::frequencyDomain(QVector<double> xAxis, QVector<double> yAxis)
{
	int FFTSize = 128;
	double frequencyRes = 44100 / FFTSize;
	auto fft = Aquila::FftFactory::getFft(FFTSize);
	auto spectrum = fft->fft(yAxis.toStdVector().data());
	QVector<double> amplitude, frequency;
	int index = 0;
	for each (auto item in spectrum)
	{
		amplitude.push_back(pow(item.real(), 2) + pow(item.imag(), 2));
		frequency.push_back((index++)*frequencyRes);
	}

	ui->FrequencyDomainPlot->clearGraphs();

	ui->FrequencyDomainPlot->addGraph();
	ui->FrequencyDomainPlot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsImpulse);
	ui->FrequencyDomainPlot->graph(0)->setData(frequency, amplitude);
	ui->FrequencyDomainPlot->xAxis->setRange(20, 20000);
	ui->FrequencyDomainPlot->yAxis->setRange(0, *std::max_element(amplitude.begin(), amplitude.end()));
	ui->FrequencyDomainPlot->replot();
}


void DSPMainWindow::plot(const QVector<double> xAxis, const QVector<double> yAxis)
{
	ui->NormalResultPlot->clearGraphs();

	ui->NormalResultPlot->addGraph();
	ui->NormalResultPlot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
	ui->NormalResultPlot->graph(0)->setData(xAxis, yAxis);
	ui->NormalResultPlot->xAxis->setRange(0, 0.1);
	ui->NormalResultPlot->yAxis->setRange(-5, 5);
	ui->NormalResultPlot->replot();
}


void DSPMainWindow::addSineWaves(QVector<double> xAxis1, QVector<double> yAxis1, QVector<double> xAxis2, QVector<double> yAxis2, QVector<double> &newXAxis, QVector<double> &newYAxis)
{

	for (int i = 0; i < 44100; i++)
	{
		newXAxis[i] = xAxis1[i];
		newYAxis[i] = yAxis1[i] + yAxis2[i];
	}

}
