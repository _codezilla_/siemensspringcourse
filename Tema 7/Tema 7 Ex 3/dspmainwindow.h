#ifndef DSPMAINWINDOW_H
#define DSPMAINWINDOW_H

#include <QMainWindow>

namespace Ui {
	class DSPMainWindow;
}

class DSPMainWindow : public QMainWindow {
	Q_OBJECT

public:
	explicit DSPMainWindow(QWidget *parent = 0);
	~DSPMainWindow();

private:
	Ui::DSPMainWindow *ui;
	void sineWave(QVector<double>& xAxis, QVector<double>& yAxis, double A, double f);
	void plot(const QVector<double> xAxis, const QVector<double> yAxis);
	
	private slots:
	void generateButtonMethod();
};

#endif // DSPMAINWINDOW_H
