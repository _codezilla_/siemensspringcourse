/********************************************************************************
** Form generated from reading UI file 'dspmainwindow.ui'
**
** Created by: Qt User Interface Compiler version 5.8.0
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_DSPMAINWINDOW_H
#define UI_DSPMAINWINDOW_H

#include <QtCore/QVariant>
#include <QtWidgets/QAction>
#include <QtWidgets/QApplication>
#include <QtWidgets/QButtonGroup>
#include <QtWidgets/QDoubleSpinBox>
#include <QtWidgets/QHBoxLayout>
#include <QtWidgets/QHeaderView>
#include <QtWidgets/QMainWindow>
#include <QtWidgets/QPushButton>
#include <QtWidgets/QVBoxLayout>
#include <QtWidgets/QWidget>
#include "qcustomplot.h"

QT_BEGIN_NAMESPACE

class Ui_DSPMainWindow
{
public:
    QWidget *centralWidget;
    QVBoxLayout *verticalLayout;
    QHBoxLayout *horizontalLayout;
    QPushButton *pushButton;
    QDoubleSpinBox *doubleSpinBox;
    QCustomPlot *plot;

    void setupUi(QMainWindow *DSPMainWindow)
    {
        if (DSPMainWindow->objectName().isEmpty())
            DSPMainWindow->setObjectName(QStringLiteral("DSPMainWindow"));
        DSPMainWindow->resize(1200, 700);
        centralWidget = new QWidget(DSPMainWindow);
        centralWidget->setObjectName(QStringLiteral("centralWidget"));
        verticalLayout = new QVBoxLayout(centralWidget);
        verticalLayout->setSpacing(5);
        verticalLayout->setContentsMargins(15, 15, 15, 15);
        verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
        horizontalLayout = new QHBoxLayout();
        horizontalLayout->setSpacing(5);
        horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
        pushButton = new QPushButton(centralWidget);
        pushButton->setObjectName(QStringLiteral("pushButton"));

        horizontalLayout->addWidget(pushButton);

        doubleSpinBox = new QDoubleSpinBox(centralWidget);
        doubleSpinBox->setObjectName(QStringLiteral("doubleSpinBox"));
        doubleSpinBox->setMinimum(600000);
        doubleSpinBox->setMaximum(1e+6);

        horizontalLayout->addWidget(doubleSpinBox);


        verticalLayout->addLayout(horizontalLayout);

        plot = new QCustomPlot(centralWidget);
        plot->setObjectName(QStringLiteral("plot"));

        verticalLayout->addWidget(plot);

        DSPMainWindow->setCentralWidget(centralWidget);

        retranslateUi(DSPMainWindow);

        QMetaObject::connectSlotsByName(DSPMainWindow);
    } // setupUi

    void retranslateUi(QMainWindow *DSPMainWindow)
    {
        DSPMainWindow->setWindowTitle(QApplication::translate("DSPMainWindow", "Main Window", Q_NULLPTR));
        pushButton->setText(QApplication::translate("DSPMainWindow", "Generate ", Q_NULLPTR));
    } // retranslateUi

};

namespace Ui {
    class DSPMainWindow: public Ui_DSPMainWindow {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_DSPMAINWINDOW_H
