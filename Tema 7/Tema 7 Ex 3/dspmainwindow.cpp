#include "dspmainwindow.h"
#include "ui_dspmainwindow.h"
#include <math.h>
#include <aquila/global.h>
#include <aquila/transform/FftFactory.h>
#include <aquila\source\generator\TriangleGenerator.h>
#include <string>
#include <iostream>

DSPMainWindow::DSPMainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::DSPMainWindow)
{
	ui->setupUi(this);

	connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(generateButtonMethod()));
}


void DSPMainWindow::generateButtonMethod()
{
	auto frequency = ui->doubleSpinBox->text().toDouble();
	QVector<double> carrierXAxis(44100);
	for (int i = 0; i < 44100; i++)
	{
		carrierXAxis[i] = i / 44100.0;
	}
	QVector<double> carrierYAxis(44100);
	sineWave(carrierXAxis, carrierYAxis, 1, frequency);



	QVector<double> modulatedXAxis(44100);
	for (int i = 0; i < 44100; i++)
	{
		modulatedXAxis[i] = i / 44100.0;
	}
	QVector<double> modulatedYAxis(44100);
	Aquila::TriangleGenerator generator(2 * frequency);
	generator.setWidth(0.5).setFrequency(frequency / 100).setAmplitude(255.0).generate(44100);
	for (auto i = 0; i < generator.getSamplesCount(); ++i)
	{
		modulatedYAxis[i] = generator.sample(i);
	}


	QVector<double> AMXAxis(44100);
	for (int i = 0; i < 44100; i++)
	{
		AMXAxis[i] = i / 44100.0;
	}
	QVector<double> AMYAxis(44100);
	for (int i = 0; i < 44100; i++)
	{
		AMYAxis[i] = (1 + modulatedYAxis[i])*carrierYAxis[i];
	}

	plot(AMXAxis, AMYAxis);

}

DSPMainWindow::~DSPMainWindow()
{
	delete ui;
}


void DSPMainWindow::sineWave(QVector<double>& xAxis, QVector<double>& yAxis, double A, double f)
{
	for (int i = 0; i < 44100; i++)
	{
		xAxis[i] = i / 44100.0;
	}

	for (int i = 0; i < 44100; i++)
	{
		yAxis[i] = A * sin(2 * M_PI * f* xAxis[i]);
	}
}


void DSPMainWindow::plot(const QVector<double> xAxis, const QVector<double> yAxis)
{
	ui->plot->clearGraphs();

	ui->plot->addGraph();
	ui->plot->graph(0)->setLineStyle(QCPGraph::LineStyle::lsLine);
	ui->plot->graph(0)->setData(xAxis, yAxis);
	ui->plot->xAxis->setRange(0, 0.1);
	ui->plot->yAxis->setRange(-300, +300);
	ui->plot->replot();
}
