#include <iostream>
#include <fstream>
int a[10];

int main() {

	a[11]++;

	int* b = new int[10];

	int x;
	x++;

	std::ifstream fin("ImNotClosingIfStreamResource");	

	return 0;
	int cifra=5;
	cifra++;
}

/*
	Array 'a[10]' accessed at index 11, which is out of bounds.
	Uninitialized variable: x
	Memory leak: b
	Variable 'cifra' is modified but its new value is never used.
	Statements following return, break, continue, goto or throw will never be executed.
*/