#pragma once
#include <spring\Framework\IScene.h>
#include <string>
#include <qwidget.h>
#include <qobject.h>
#include <qlabel.h>
#include <qapplication.h>
#include <qboxlayout.h>
namespace Spring {

	class InitialScene : public IScene {
		Q_OBJECT
	public:
		QWidget *centralWidget;
		explicit InitialScene(const std::string& ac_szSceneName);
		virtual void createScene() override;
		virtual void release() override;
		void createGUI();

		QLabel *label;
		QVBoxLayout *verticalLayout;

	};
}
