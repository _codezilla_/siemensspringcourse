#include "..\..\..\include\spring\Application\InitialScene.h"

#include <iostream>
namespace Spring {
	InitialScene::InitialScene(const std::string & ac_szSceneName) :IScene(ac_szSceneName)
	{
	}
	void InitialScene::createScene()
	{
		createGUI();
	}

	void InitialScene::release()
	{
		delete centralWidget;
	}
	void InitialScene::createGUI()
	{
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		verticalLayout = new QVBoxLayout(centralWidget);
		verticalLayout->setSpacing(6);
		verticalLayout->setContentsMargins(11, 11, 11, 11);
		verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
		label = new QLabel(centralWidget);
		label->setObjectName(QStringLiteral("label"));
		label->setAlignment(Qt::AlignCenter);

		verticalLayout->addWidget(label);


		m_uMainWindow->setCentralWidget(centralWidget);

		label->setText(QApplication::translate("MainWindow", "Hello World", Q_NULLPTR));
	}
}
