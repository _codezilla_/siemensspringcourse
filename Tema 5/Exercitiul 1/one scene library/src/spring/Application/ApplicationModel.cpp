#include "..\..\..\include\spring\Application\ApplicationModel.h"
#include <spring\Application\InitialScene.h>
const std::string initialSceneName = "InitialScene";

namespace Spring {
	CApplicationModel::CApplicationModel() : IApplicationModel()
	{

	}
	void CApplicationModel::defineScene()
	{
		IScene* initialScene = new InitialScene(initialSceneName);
		m_Scenes.emplace(initialSceneName, initialScene);

	}
	void CApplicationModel::defineInitialScene()
	{
		mv_szInitialScene = initialSceneName;
	}
	void CApplicationModel::defineTransientData()
	{

	}
}
