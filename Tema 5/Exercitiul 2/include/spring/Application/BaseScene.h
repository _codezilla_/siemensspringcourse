#pragma once
#include <spring\Framework\IScene.h>
#include <qwidget.h>
#include <qobject.h>
#include <qgridlayout.h>
#include <qpushbutton>
#include <qlabel.h>
#include <qapplication.h>
#include <string>

namespace Spring {
	class BaseScene :public IScene {
		Q_OBJECT
	public:
		QWidget *centralWidget;
		explicit BaseScene(const std::string& ac_szSceneName);
		virtual void createScene() override;
		virtual void release() override;
		void createGUI();


		QVBoxLayout *verticalLayout_2;
		QSpacerItem *verticalSpacer_2;
		QLabel *label;
		QSpacerItem *verticalSpacer;
		QHBoxLayout *horizontalLayout_2;
		QSpacerItem *horizontalSpacer;
		QPushButton *pushButton;
		QSpacerItem *horizontalSpacer_4;
		QSpacerItem *horizontalSpacer_3;
		QSpacerItem *horizontalSpacer_2;

		private slots:
		void backButtonClick();

	};
}
