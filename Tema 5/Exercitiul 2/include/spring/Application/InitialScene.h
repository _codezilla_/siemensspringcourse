#pragma once
#include <spring\Framework\IScene.h>
#include <qwidget.h>
#include <qobject.h>
#include <qgridlayout.h>
#include <qpushbutton>
#include <qlineedit.h>
#include <qapplication.h>
#include <qpushbutton.h>
#include <string>

namespace Spring {

	class InitialScene : public IScene {
		Q_OBJECT
	public:
		QWidget *centralWidget;
		explicit InitialScene(const std::string& ac_szSceneName);
		virtual void createScene() override;
		virtual void release() override;
		void createGUI();

		QVBoxLayout *verticalLayout;
		QSpacerItem *verticalSpacer;
		QHBoxLayout *horizontalLayout_2;
		QSpacerItem *horizontalSpacer_3;
		QLineEdit *lineEdit;
		QSpacerItem *horizontalSpacer_4;
		QHBoxLayout *horizontalLayout;
		QSpacerItem *horizontalSpacer;
		QSpacerItem *verticalSpacer_3;
		QPushButton *pushButton;
		QSpacerItem *horizontalSpacer_2;
		QSpacerItem *verticalSpacer_2;

		private slots :
		void sayHelloButton();
	};
}
