#include "..\..\..\include\spring\Application\ApplicationModel.h"
#include <spring\Application\BaseScene.h>

const std::string initialSceneName = "InitialScene";

namespace Spring {
	CApplicationModel::CApplicationModel() : IApplicationModel()
	{
	}
	void CApplicationModel::defineScene()
	{
		IScene* initialScene = new InitialScene(initialSceneName);
		m_Scenes.emplace(initialSceneName, initialScene);

		IScene* baseScene = new BaseScene("BaseScene");
		m_Scenes.emplace("BaseScene", baseScene);
	}
	void CApplicationModel::defineInitialScene()
	{
		mv_szInitialScene = initialSceneName;
	}
	void CApplicationModel::defineTransientData()
	{
		m_TransientData.emplace("Name", std::string("Default Name"));
	}
}
