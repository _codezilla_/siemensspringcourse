#include "..\..\..\include\spring\Application\InitialScene.h"

namespace Spring {
	InitialScene::InitialScene(const std::string & ac_szSceneName) :IScene(ac_szSceneName)
	{
	}

	void InitialScene::createScene()
	{
		createGUI();
	}

	void InitialScene::release()
	{
		delete centralWidget;
	}

	void InitialScene::sayHelloButton()
	{
		m_TransientDataCollection.erase("Name");
		m_TransientDataCollection.emplace("Name", lineEdit->text().toStdString());
		emit SceneChange("BaseScene");
	}
	void InitialScene::createGUI()
	{
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		verticalLayout = new QVBoxLayout(centralWidget);
		verticalLayout->setSpacing(6);
		verticalLayout->setContentsMargins(11, 11, 11, 11);
		verticalLayout->setObjectName(QStringLiteral("verticalLayout"));
		verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		verticalLayout->addItem(verticalSpacer);

		horizontalLayout_2 = new QHBoxLayout();
		horizontalLayout_2->setSpacing(6);
		horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
		horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		horizontalLayout_2->addItem(horizontalSpacer_3);


		lineEdit = new QLineEdit(centralWidget);
		lineEdit->setObjectName(QStringLiteral("lineEdit"));
		std::string name = boost::any_cast<std::string>(m_TransientDataCollection["Name"]);
		lineEdit->setText(QApplication::translate("MainWindow", name.c_str(), Q_NULLPTR));


		horizontalLayout_2->addWidget(lineEdit);

		horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		horizontalLayout_2->addItem(horizontalSpacer_4);


		verticalLayout->addLayout(horizontalLayout_2);

		horizontalLayout = new QHBoxLayout();
		horizontalLayout->setSpacing(6);
		horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		horizontalLayout->addItem(horizontalSpacer);

		verticalSpacer_3 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		horizontalLayout->addItem(verticalSpacer_3);

		pushButton = new QPushButton(centralWidget);
		pushButton->setObjectName(QStringLiteral("pushButton"));
		connect(pushButton, SIGNAL(clicked()), this, SLOT(sayHelloButton()));

		horizontalLayout->addWidget(pushButton);

		horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		horizontalLayout->addItem(horizontalSpacer_2);


		verticalLayout->addLayout(horizontalLayout);

		verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		verticalLayout->addItem(verticalSpacer_2);

		m_uMainWindow->setCentralWidget(centralWidget);


		pushButton->setText(QApplication::translate("MainWindow", "Say Hello!", Q_NULLPTR));
	}
}
