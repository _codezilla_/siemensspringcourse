#include "..\..\..\include\spring\Application\BaseScene.h"

namespace Spring {
	BaseScene::BaseScene(const std::string & ac_szSceneName) :IScene(ac_szSceneName)
	{
	}

	void BaseScene::createScene()
	{
		createGUI();
	}

	void BaseScene::release()
	{
		delete centralWidget;
	}

	void BaseScene::backButtonClick()
	{
		emit SceneChange("InitialScene");
	}

	void BaseScene::createGUI()
	{

		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		verticalLayout_2 = new QVBoxLayout(centralWidget);
		verticalLayout_2->setSpacing(6);
		verticalLayout_2->setContentsMargins(11, 11, 11, 11);
		verticalLayout_2->setObjectName(QStringLiteral("verticalLayout_2"));
		verticalSpacer_2 = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		verticalLayout_2->addItem(verticalSpacer_2);

		label = new QLabel(centralWidget);
		label->setObjectName(QStringLiteral("label"));
		label->setAlignment(Qt::AlignCenter);

		verticalLayout_2->addWidget(label);

		verticalSpacer = new QSpacerItem(20, 40, QSizePolicy::Minimum, QSizePolicy::Expanding);

		verticalLayout_2->addItem(verticalSpacer);

		horizontalLayout_2 = new QHBoxLayout();
		horizontalLayout_2->setSpacing(6);
		horizontalLayout_2->setObjectName(QStringLiteral("horizontalLayout_2"));
		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		horizontalLayout_2->addItem(horizontalSpacer);

		pushButton = new QPushButton(centralWidget);
		pushButton->setObjectName(QStringLiteral("pushButton"));
		connect(pushButton, SIGNAL(clicked()), this, SLOT(backButtonClick()));

		horizontalLayout_2->addWidget(pushButton);

		horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		horizontalLayout_2->addItem(horizontalSpacer_4);

		horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		horizontalLayout_2->addItem(horizontalSpacer_3);

		horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		horizontalLayout_2->addItem(horizontalSpacer_2);


		verticalLayout_2->addLayout(horizontalLayout_2);

		m_uMainWindow->setCentralWidget(centralWidget);


		auto name = boost::any_cast<std::string>(m_TransientDataCollection["Name"]);
		std::string helloMessage = "Hello, ";
		helloMessage.append(name);
		helloMessage.append(" !");
		label->setText(QApplication::translate("MainWindow", helloMessage.c_str(), Q_NULLPTR));
		pushButton->setText(QApplication::translate("MainWindow", "Go Back", Q_NULLPTR));
	}

}
