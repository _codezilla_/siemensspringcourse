#pragma once
#include <string>
#include <spring\Framework\IScene.h>
#include <qwidget.h>
#include <qobject.h>
#include <qgridlayout.h>
#include <qpushbutton>
#include <qlineedit.h>
#include <qapplication.h>

namespace Spring {
	class BaseScene :public IScene {
		Q_OBJECT
	public:
		QWidget *centralWidget;
		explicit BaseScene(const std::string& ac_szSceneName);
		virtual void createScene() override;
		virtual void release() override;
		void createGUI();


		QHBoxLayout *horizontalLayout;
		QSpacerItem *horizontalSpacer;
		QPushButton *previousButton;
		QSpacerItem *horizontalSpacer_3;
		QLineEdit *lineEdit;
		QSpacerItem *horizontalSpacer_4;
		QPushButton *nextButton;
		QSpacerItem *horizontalSpacer_2;

		private slots:
		void previousButtonClick();
		void nextButtonClick();
	};
}
