#include "..\..\..\include\spring\Application\ApplicationModel.h"
#include <spring\Application\BaseScene.h>

const std::string initialSceneName = "BaseScene0";

namespace Spring {
	CApplicationModel::CApplicationModel() : IApplicationModel()
	{
	}
	void CApplicationModel::defineScene()
	{
		for (auto i = 0; i < 1000; i++) {
			std::string sceneName = "BaseScene";
			sceneName.append(std::to_string(i));
			const std::string toPass = sceneName;
			IScene* baseScene = new BaseScene(toPass);
			m_Scenes.emplace(toPass, baseScene);
		}
	}
	void CApplicationModel::defineInitialScene()
	{
		mv_szInitialScene = initialSceneName;
	}
	void CApplicationModel::defineTransientData()
	{
		m_TransientData.emplace("SceneNumber", 0);
	}
}
