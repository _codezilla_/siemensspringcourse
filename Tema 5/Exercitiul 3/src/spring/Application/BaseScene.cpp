#include "..\..\..\include\spring\Application\BaseScene.h"

namespace Spring {
	BaseScene::BaseScene(const std::string & ac_szSceneName) :IScene(ac_szSceneName)
	{

	}
	void BaseScene::createScene()
	{
		createGUI();
	}
	void BaseScene::release()
	{
		delete centralWidget;
	}

	void BaseScene::previousButtonClick()
	{
		if (sGetSceneName() != "BaseScene0") {
			std::string currentSceneName = sGetSceneName();
			int x = boost::any_cast<int>(m_TransientDataCollection["SceneNumber"]);

			x--;
			std::string nextScene = "BaseScene";
			nextScene.append(std::to_string(x));
			m_TransientDataCollection.erase("SceneNumber");
			m_TransientDataCollection.emplace("SceneNumber", x);
			emit SceneChange(nextScene);
		}
	}
	void BaseScene::nextButtonClick()
	{
		if (sGetSceneName() != "BaseScene999") {
			std::string currentSceneName = sGetSceneName();
			int x = boost::any_cast<int>(m_TransientDataCollection["SceneNumber"]);
			x++;
			std::string nextScene = "BaseScene";
			nextScene.append(std::to_string(x));
			m_TransientDataCollection.erase("SceneNumber");
			m_TransientDataCollection.emplace("SceneNumber", x);
			emit SceneChange(nextScene);
		}
	}

	void BaseScene::createGUI()
	{
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		horizontalLayout = new QHBoxLayout(centralWidget);
		horizontalLayout->setSpacing(6);
		horizontalLayout->setContentsMargins(11, 11, 11, 11);
		horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		horizontalLayout->addItem(horizontalSpacer);

		previousButton = new QPushButton(centralWidget);
		previousButton->setObjectName(QStringLiteral("previousButton"));
		connect(previousButton, SIGNAL(clicked()), this, SLOT(previousButtonClick()));

		horizontalLayout->addWidget(previousButton);

		horizontalSpacer_3 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		horizontalLayout->addItem(horizontalSpacer_3);

		lineEdit = new QLineEdit(centralWidget);
		lineEdit->setObjectName(QStringLiteral("lineEdit"));


		auto number = boost::any_cast<int>(m_TransientDataCollection["SceneNumber"]);
		lineEdit->setText(QApplication::translate("MainWindow", std::to_string(number).c_str(), Q_NULLPTR));


		horizontalLayout->addWidget(lineEdit);

		horizontalSpacer_4 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		horizontalLayout->addItem(horizontalSpacer_4);

		nextButton = new QPushButton(centralWidget);
		nextButton->setObjectName(QStringLiteral("nextButton"));
		connect(nextButton, SIGNAL(clicked()), this, SLOT(nextButtonClick()));

		horizontalLayout->addWidget(nextButton);

		horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		horizontalLayout->addItem(horizontalSpacer_2);

		m_uMainWindow->setCentralWidget(centralWidget);

		previousButton->setText(QApplication::translate("MainWindow", "Previous", Q_NULLPTR));
		nextButton->setText(QApplication::translate("MainWindow", "Next", Q_NULLPTR));
	}

}
