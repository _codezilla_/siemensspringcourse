#include <spring\Application\ApplicationModel.h>
#include <spring\Framework\Application.h>
#include <qapplication.h>

int main(int argc, char **argv)
{
	QApplication app(argc, argv);
	Spring::Application& application = Spring::Application::getInstance();
	Spring::CApplicationModel model;

	application.setApplicationModel(&model);
	application.start("1000 scene exercise", 400, 400);

	app.exec();


	return 0;
}