#include "spring/Application/MonoInput.h"
#include <qendian.h>
#include <iostream>
#include <qaudiodeviceinfo.h>

MonoInput::MonoInput(double timeSlice, unsigned sampleRate, QAudioFormat::Endian endian, int byte)
	:dataLength(timeSlice * sampleRate),
	channelBytes(byte / 8)
{
	this->endian = endian;
	this->byte = byte;



	audioFormat.setSampleRate(sampleRate);
	audioFormat.setChannelCount(1);
	audioFormat.setSampleSize(byte);
	audioFormat.setCodec("audio/pcm");

	audioFormat.setByteOrder(endian);

	QAudioDeviceInfo info = QAudioDeviceInfo::defaultInputDevice();
	if (!info.isFormatSupported(audioFormat))
		audioFormat.setByteOrder(info.nearestFormat(audioFormat).byteOrder());
	//audioFormat = info.nearestFormat(audioFormat);



	audioFormat.setSampleType(QAudioFormat::SampleType::SignedInt);

	maxAmplitude = 32767;

	std::cout << audioFormat.sampleRate() << " " << audioFormat.channelCount() << " " << audioFormat.sampleSize() << " " << audioFormat.sampleType() << " " << audioFormat.codec().toStdString();
}


MonoInput::~MonoInput()
{
}

qint64 MonoInput::writeData(const char *data, qint64 len)
{
	const auto *ptr = data;

	for (auto i = 0; i < len / channelBytes; ++i)
	{
		float level;

		if (endian == QAudioFormat::LittleEndian)
		{
			if (byte == 16)
			{
				qint16 value = qFromLittleEndian<qint16>(ptr);
				level = float(value) * (5. / maxAmplitude);
			}
			else
			{
				qint32 value = qFromLittleEndian<qint32>(ptr);
				level = float(value) * (5. / maxAmplitude);
			}

		}
		else
		{
			if (byte == 16)
			{
				qint16 value = qFromBigEndian<qint16>(ptr);
				level = float(value) * (5. / maxAmplitude);
			}
			else
			{
				qint32 value = qFromBigEndian<qint32>(ptr);
				level = float(value) * (5. / maxAmplitude);
			}

		}
		timeData.push_back(level);
		ptr += channelBytes;
	}

	if (timeData.size() > dataLength)
	{
		timeData.remove(0, timeData.size() - dataLength);
	}

	std::cout << "data received " << len << std::endl;
	emit readyRead();
	return len;
}


QAudioFormat MonoInput::getAudioFormat()
{
	return audioFormat;
}

QVector<double> MonoInput::vecGetData() const
{
	return timeData;
}