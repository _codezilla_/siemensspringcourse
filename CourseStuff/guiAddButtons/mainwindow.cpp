#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qpushbutton.h>
#include <qboxlayout.h>

MainWindow::MainWindow(QWidget *parent) :
	QMainWindow(parent),
	ui(new Ui::MainWindow)
{
	ui->setupUi(this);

	QPushButton *button = new QPushButton(ui->centralWidget);
	button->setText("Add New Button");
	connect(button, SIGNAL(clicked()), this, SLOT(buttonClicked()));
	QHBoxLayout *layout = new QHBoxLayout();
	layout->addWidget(button);
	ui->centralWidget->setLayout(layout);

}

MainWindow::~MainWindow()
{
	delete ui;
}

void MainWindow::buttonClicked()
{
	QPushButton *newButton = new QPushButton(ui->centralWidget);
	newButton->setText("I was added");
	ui->centralWidget->layout()->addWidget(newButton);

}