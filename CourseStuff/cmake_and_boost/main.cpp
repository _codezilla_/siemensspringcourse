#include <iostream>
#include <boost/any.hpp>

void printAny(boost::any anything)
{
	if (anything.type() == typeid(const char*))
		std::cout << boost::any_cast<const char*>(anything) << std::endl;
	else if (anything.type() == typeid(int))
		std::cout << boost::any_cast<int>(anything) << std::endl;
}


class Book
{
public:
	Book(int);
};


int main()
{
	boost::any b = 6;
	auto x = 6;
	std::cin >> x;
	std::cout << x;


	
	


	//printAny(b);
	return 0;

}
