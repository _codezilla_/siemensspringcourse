#pragma once
#include <memory>
#include <QtWidgets/qmainwindow.h>
#include <Spring/Framework/global.h>
#include <boost/any.hpp>
#include <fstream>
#include <iostream>
namespace Spring
{
	  

	class Framework_EXPORT_IMPORT_API IScene : public QObject
	{
		
		Q_OBJECT

			signals :
		void SceneChange(const std::string& ac_szSceneName);

	public:
		explicit IScene(const std::string& ac_szSceneName) : mc_szSceneName(ac_szSceneName)
		{	
			;
		}

		bool saveTransientData(std::string filename) {
			std::ofstream fout(filename);
			for (const auto& current : m_TransientDataCollection) {
				fout << current.first;
				fout << " ";
				const int* integer = nullptr;
				const unsigned int* unsigned_int = nullptr;
				const float* floatingPoint = nullptr;
				const double* doubleFloat = nullptr;
				const std::string string;
				if (integer = boost::any_cast<int>(&current.second))
					fout << std::to_string(*integer);
				else if (unsigned_int = boost::any_cast<unsigned>(&current.second))
					fout << std::to_string(*unsigned_int);
				else if (floatingPoint = boost::any_cast<float>(&current.second))
					fout << std::to_string(*floatingPoint);
				else if (doubleFloat = boost::any_cast<double>(&current.second))
					fout << std::to_string(*doubleFloat);
				else 
					fout << *(boost::any_cast<std::string>(&current.second));
				fout << std::endl;
			}
			fout.close();
			return true;

		}

		virtual void createScene() = 0;

		virtual void drawScene()
		{
			QMetaObject::connectSlotsByName(m_uMainWindow.get());
		}

		virtual void release() = 0;

		virtual void setWindow(std::unique_ptr<QMainWindow>& av_xMainWindow)
		{
			m_uMainWindow = std::move(av_xMainWindow);
		}

		virtual std::unique_ptr<QMainWindow>& uGetWindow()
		{
			return m_uMainWindow;
		}

		virtual void setTransientData(std::map<const std::string,  boost::any>& ac_xTransientData)
		{
			m_TransientDataCollection = std::move(ac_xTransientData);
		}


		virtual std::map<const std::string,  boost::any> uGetTransientData()
		{
			return m_TransientDataCollection;
		}

		void show() const
		{
			m_uMainWindow->show();
		}

		virtual ~IScene()
		{
			;
		}

		virtual std::string sGetSceneName()
		{
			return mc_szSceneName;
		}
		
		typedef std::shared_ptr<IScene> s_ptr;
		typedef std::unique_ptr<IScene> u_ptr;

	protected:
		std::unique_ptr<QMainWindow> m_uMainWindow;
		std::map<const std::string, boost::any> m_TransientDataCollection;

	private:
		const std::string mc_szSceneName;

	};


}
