#pragma once
#include <spring\Framework\IScene.h>

#include <qgridlayout.h>
#include <qboxlayout.h>
#include <qpushbutton.h>
#include <qapplication.h>
#include <qcustomplot.h>

namespace Spring {
	class BaseScene : public IScene
	{

	public:

		explicit BaseScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;
		void MakeGUI();
		~BaseScene();

		QWidget *centralWidget;
		QCustomPlot *qCustomPlot;
		QGridLayout *gridLayout;
		QHBoxLayout *horizontalLayout;
		QSpacerItem *horizontalSpacer;
		QPushButton *pushButton_2;
		QPushButton *pushButton;

	};

}
