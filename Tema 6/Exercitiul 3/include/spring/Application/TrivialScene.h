#pragma once
#include <spring\Framework\IScene.h>

#include <qgridlayout.h>
#include <qapplication.h>
#include <qlabel.h>
#include <memory>
#include <TrivialClass.h>

namespace Spring {
	class TrivialScene : public IScene {
	public:
		explicit TrivialScene(const std::string& ac_szSceneName);

		void createScene() override;

		void release() override;
		void MakeGUI();

		~TrivialScene();
		QWidget *centralWidget;
		QGridLayout *gridLayout;
		QSpacerItem *horizontalSpacer;
		QSpacerItem *horizontalSpacer_2;
		QLabel *label;

	private:
		std::unique_ptr<TrivialClass::TrivialClass> trivialClass;
	};

}