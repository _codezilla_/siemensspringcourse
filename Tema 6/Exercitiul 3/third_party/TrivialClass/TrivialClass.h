#pragma once

namespace TrivialClass {

	class __declspec(dllexport) TrivialClass {
	private:
		int sampleRate;
		double displayTime;
	public:
		TrivialClass(const int sampleRate, double displayTime);
		double getSampleNumber();
	};
}