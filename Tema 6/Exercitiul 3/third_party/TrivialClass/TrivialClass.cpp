#include "TrivialClass.h"

namespace TrivialClass {
	TrivialClass::TrivialClass(const int sampleRate, const  double displayTime)
	{
		this->sampleRate = sampleRate;
		this->displayTime = displayTime;
	}

	double TrivialClass::getSampleNumber()
	{
		return sampleRate*displayTime;
	}
}