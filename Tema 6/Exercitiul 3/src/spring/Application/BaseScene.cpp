#include "..\..\..\include\spring\Application\BaseScene.h"

namespace Spring {
	BaseScene::BaseScene(const std::string& ac_szSceneName) : IScene(ac_szSceneName)
	{
	}

	void BaseScene::createScene()
	{
		std::string appName = boost::any_cast<std::string>(m_TransientDataCollection["ApplicationName"]);

		m_uMainWindow->setWindowTitle(QString(appName.c_str()));
		MakeGUI();
	}

	void BaseScene::release()
	{

	}

	void BaseScene::MakeGUI()
	{



		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		gridLayout = new QGridLayout(centralWidget);
		gridLayout->setSpacing(6);
		gridLayout->setContentsMargins(11, 11, 11, 11);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));

		qCustomPlot = new QCustomPlot(centralWidget);
		qCustomPlot->setObjectName(QStringLiteral("qCustomPlot"));

		//listView = new QListView(centralWidget);
		//listView->setObjectName(QStringLiteral("listView"));

		gridLayout->addWidget(qCustomPlot, 0, 1, 1, 1);

		//gridLayout->addWidget(listView, 0, 1, 1, 1);

		horizontalLayout = new QHBoxLayout();
		horizontalLayout->setSpacing(6);
		horizontalLayout->setObjectName(QStringLiteral("horizontalLayout"));
		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		horizontalLayout->addItem(horizontalSpacer);

		pushButton_2 = new QPushButton(centralWidget);
		pushButton_2->setObjectName(QStringLiteral("pushButton_2"));

		horizontalLayout->addWidget(pushButton_2);

		pushButton = new QPushButton(centralWidget);
		pushButton->setObjectName(QStringLiteral("pushButton"));

		horizontalLayout->addWidget(pushButton);


		gridLayout->addLayout(horizontalLayout, 1, 1, 1, 1);

		m_uMainWindow->setCentralWidget(centralWidget);

		pushButton_2->setText(QApplication::translate("MainWindow", "Start", Q_NULLPTR));
		pushButton->setText(QApplication::translate("MainWindow", "Stop", Q_NULLPTR));
	}



	BaseScene::~BaseScene()
	{
	}
}
