#include "spring\Application\TrivialScene.h"

namespace Spring {


	Spring::TrivialScene::TrivialScene(const std::string & ac_szSceneName) : IScene(ac_szSceneName)
	{

	}
	void TrivialScene::createScene()
	{
		std::string appName = boost::any_cast<std::string>(m_TransientDataCollection["ApplicationName"]);


		trivialClass = std::unique_ptr<TrivialClass::TrivialClass>(new TrivialClass::TrivialClass(100, 0.2));


		m_uMainWindow->setWindowTitle(QString(appName.c_str()));
		MakeGUI();
	}
	void TrivialScene::release()
	{
	}
	void TrivialScene::MakeGUI()
	{
		centralWidget = new QWidget(m_uMainWindow.get());
		centralWidget->setObjectName(QStringLiteral("centralWidget"));
		gridLayout = new QGridLayout(centralWidget);
		gridLayout->setSpacing(6);
		gridLayout->setContentsMargins(11, 11, 11, 11);
		gridLayout->setObjectName(QStringLiteral("gridLayout"));
		horizontalSpacer = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer, 0, 0, 1, 1);

		horizontalSpacer_2 = new QSpacerItem(40, 20, QSizePolicy::Expanding, QSizePolicy::Minimum);

		gridLayout->addItem(horizontalSpacer_2, 0, 2, 1, 1);

		label = new QLabel(centralWidget);
		label->setObjectName(QStringLiteral("label"));

		gridLayout->addWidget(label, 0, 1, 1, 1);

		m_uMainWindow->setCentralWidget(centralWidget);

		std::string text = std::to_string(trivialClass->getSampleNumber());

		label->setText(QApplication::translate("MainWindow", text.c_str(), Q_NULLPTR));
	}
	TrivialScene::~TrivialScene()
	{
	}

}

