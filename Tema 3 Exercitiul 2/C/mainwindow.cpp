#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qlineedit.h>
#include <qwidget.h>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_pushButton_clicked()
{
    QLineEdit *lineEdit = new QLineEdit();
    ui->centralWidget->layout()->addWidget(lineEdit);
}
