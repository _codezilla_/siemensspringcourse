//Please ignore the duplicate code and empty functions
#include "mainwindow.h"
#include "ui_mainwindow.h"
#include <qcolor.h>
#include <QPalette>
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::on_horizontalSlider_5_sliderMoved(int position)
{

}

void MainWindow::on_horizontalSlider_4_sliderMoved(int position)
{

}

void MainWindow::on_horizontalSlider_3_sliderMoved(int position)
{

}

void MainWindow::on_horizontalSlider_3_valueChanged(int value)
{
    QColor myColor(ui->horizontalSlider_5->value(),ui->horizontalSlider_4->value(),ui->horizontalSlider_3->value());
    QPalette newPalette = ui->centralWidget->palette();
    newPalette.setColor(QPalette::ColorRole::Background, myColor);
    ui->centralWidget->setAutoFillBackground(true);
    ui->centralWidget->setPalette(newPalette);
    ui->centralWidget->show();
}

void MainWindow::on_horizontalSlider_4_valueChanged(int value)
{
    QColor myColor(ui->horizontalSlider_5->value(),ui->horizontalSlider_4->value(),ui->horizontalSlider_3->value());
    QPalette newPalette = ui->centralWidget->palette();
    newPalette.setColor(QPalette::ColorRole::Background, myColor);
    ui->centralWidget->setAutoFillBackground(true);
    ui->centralWidget->setPalette(newPalette);
    ui->centralWidget->show();
}

void MainWindow::on_horizontalSlider_5_valueChanged(int value)
{
    QColor myColor(ui->horizontalSlider_5->value(),ui->horizontalSlider_4->value(),ui->horizontalSlider_3->value());
    QPalette newPalette = ui->centralWidget->palette();
    newPalette.setColor(QPalette::ColorRole::Background, myColor);
    ui->centralWidget->setAutoFillBackground(true);
    ui->centralWidget->setPalette(newPalette);
    ui->centralWidget->show();
}
