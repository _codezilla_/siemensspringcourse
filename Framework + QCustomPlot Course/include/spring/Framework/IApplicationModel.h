#pragma once
#include <Spring/Framework/IScene.h>
#include <Spring/Framework/global.h>
#include <fstream>
namespace Spring
{

	class Framework_EXPORT_IMPORT_API IApplicationModel
	{
	public:
		enum ExpectedType {
			Integer,
			String,
			Float,
			Double,
			Unsigned_Integer
		};

		virtual bool loadTransientData(std::string filename, std::vector<ExpectedType> expectedTypes) {
			std::ifstream fin(filename);
			if (fin.good())
			{
				char currentLine[100];
				int dataIndex = 0;
				while (fin.getline(currentLine, 100)) {
					char * pch;
					pch = strtok(currentLine, " ");
					std::string key(pch);
					pch = strtok(NULL, " ");
					std::string value(pch);
					ExpectedType valueType = expectedTypes.at(dataIndex);

					switch (valueType) {
					case String:
						m_TransientData[key] = value;
						break;
					case Integer:
						m_TransientData[key] = std::stoi(value);
						break;
					case Float:
						m_TransientData[key] = std::stof(value);
						break;
					case Double:
						m_TransientData[key] = std::stod(value);
						break;
					case Unsigned_Integer:
						m_TransientData[key] = (unsigned)std::stoi(value);
						break;
					}
					dataIndex++;
				}
				fin.close();
				return true;
			}
			fin.close();
			return false;
		}


		virtual void defineScene() = 0;

		virtual void defineInitialScene() = 0;

		virtual void defineTransientData() = 0;

		

		virtual const std::string& sGetInitialSceneName()
		{
			return mv_szInitialScene;
		}

		std::map<const std::string, IScene*> getScenes() const
		{
			return m_Scenes;
		}

		std::map<const std::string, boost::any> getTransientData() const
		{
			return m_TransientData;
		}

		IScene* getInitialScene() const
		{
			return m_Scenes.find(mv_szInitialScene)->second;
		}

		virtual ~IApplicationModel()
		{
			;
		}

	protected:
		std::string mv_szInitialScene;

		std::map<const std::string, IScene*> m_Scenes;
		std::map<const std::string, boost::any> m_TransientData;
	};

}
